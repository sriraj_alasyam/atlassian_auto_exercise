# README #

A detailed README is provided in the project folder.

### What is this repository for? ###

* Repository created for Atlassian Automation exercise for the below cases:
1. New issue creation
2. Updating an existing issue
3. Searching for an existing issue

### How do I get set up? ###

* Summary of set up: 
Import project 'selenium-automation-exercise' into an Eclipse workspace

* Configuration:
Test Environment:
OS: Windows 8.1 64-bit
Java: JDK SE 8
Browser: Firefox ver 32.0.2
Eclipse: Eclipse IDE for Java Developers (Version: Luna Release (4.4.0))

* Dependencies:
Provided required third-party libraries in project's /lib/ folder:
junit-4.11.jar
hamcrest-core-1.3.jar
selenium-server-standalone-2.43.1.jar;

* How to run tests: 
Run JiraTests test suite (Run As > JUnit Test)


### Who do I talk to? ###

* Repo owner or admin