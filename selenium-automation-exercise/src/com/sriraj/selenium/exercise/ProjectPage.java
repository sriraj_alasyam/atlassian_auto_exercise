package com.sriraj.selenium.exercise;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProjectPage {

	private static final String DATA_ISSUE_KEY = "data-issue-key";
	private static final String HREF = "href";
	private static final String CREATED_LINK_A = ".//*[@id='jira']/div[5]/div/a";
	private static final String CREATE_ISSUE_SUBMIT = "create-issue-submit";
	private static final String DESCRIPTION = "description";
	private static final String SUMMARY = "summary";
	private static final String CREATE_LINK = "create_link";
	
	private WebDriver webDriver;
	
	public ProjectPage(WebDriver wd) {
		webDriver = wd;
	}
	
	/**
	 * 
	 * @param summary
	 * @param description
	 * @return
	 */
	public String createNewIssue(String summary, String description) {
		// 1. Navigate to Project Page https://jira.atlassian.com/browse/TST
		//webDriver.navigate().to(PROJECT_TST_PAGE);
		// 2. Click on "Create"
		WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
		webDriver.findElement(By.id(CREATE_LINK)).click();
		// 3. Wait for the "Create Issue" Dialog to load
		// 4. Verify presence of input field wit id = "summary"
		webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(SUMMARY)));
		// 5. Provide Summary field input
		webDriver.findElement(By.id(SUMMARY)).sendKeys(summary);
		// 6. Provide Description field input
		webDriver.findElement(By.id(DESCRIPTION)).sendKeys(description);
		// 7. Click on Create button
		webDriver.findElement(By.id(CREATE_ISSUE_SUBMIT)).click();
		// .//*[@id='jira']/div[5]/div/a
		try {
			webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(CREATED_LINK_A)));
		} catch(TimeoutException toe) {
			System.out.println(toe.getMessage());
			return null;
		}
		webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(CREATED_LINK_A)));
		String issueLink = webDriver.findElement(By.xpath(CREATED_LINK_A)).getAttribute(HREF);
		System.out.println("Issue link = " + issueLink);
		String dataIssueKey = webDriver.findElement(By.xpath(CREATED_LINK_A)).getAttribute(DATA_ISSUE_KEY);
		System.out.println("Data Issue Key = " + dataIssueKey);
		
		return dataIssueKey;
	}

	/**
	 * 
	 * @return
	 */
	public String getErrorMessage() {
		return webDriver.findElement(By.xpath(".//div[@class='error' and @data-field='summary']")).getText();
	}

	public JiraIssuePage search(String issueKey) {
		WebElement quickSearchInput = webDriver.findElement(By.id("quickSearchInput"));
		quickSearchInput.sendKeys(issueKey);
		quickSearchInput.sendKeys(Keys.ENTER);
		return new JiraIssuePage(webDriver);
	}
}
