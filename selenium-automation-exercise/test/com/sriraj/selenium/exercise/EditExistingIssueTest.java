package com.sriraj.selenium.exercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class EditExistingIssueTest extends TestBase  {

	private static final String ISSUE_PRIORITY = "Critical";
	private static final String ISSUE_TYPE_TASK = "Task";

	@Test
	public void testEditExistingIssue() {
		ProjectPage projectPage = loginToJiraProject();
		String issueKey = projectPage.createNewIssue(ISSUE_SUMMARY, ISSUE_DESCRIPTION);
		assertNotNull(issueKey);
		
		JiraIssuePage jiraIssuePage = projectPage.search(issueKey);
		assertEquals(issueKey, jiraIssuePage.getIssueKey());
		jiraIssuePage.edit(EDITED_ISSUE_SUMMARY, ISSUE_TYPE_TASK, ISSUE_PRIORITY);
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JiraIssuePage editedJiraIssuePage = new JiraIssuePage(webDriver);
		editedJiraIssuePage.open(issueKey);
		
		
		String actualSummary = editedJiraIssuePage.getSummary();
		String actualPriority = editedJiraIssuePage.getPriority();
		String actualType = editedJiraIssuePage.getType();
		
		System.out.println("Edited Summary = " + actualSummary);
		System.out.println("Edited Priority = " + actualPriority);
		System.out.println("Edited Type = " + actualType);
		
		assertEquals(EDITED_ISSUE_SUMMARY, actualSummary);
		assertEquals(ISSUE_PRIORITY, actualPriority);
		assertEquals(ISSUE_TYPE_TASK, actualType);
	}
}
