package com.sriraj.selenium.exercise;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JiraIssuePage {
	
	private static final String ISSUE_TYPE_VAL_ID = ".//*[@id='type-val']";
	private static final String ISSUE_PRIORITY_VAL_ID = ".//*[@id='priority-val']";
	private static final String EDIT_ISSUE_SUBMIT_LINK = "edit-issue-submit";
	private static final String EDIT_ISSUE_LINK = "edit-issue";
	private static final String KEY_VAL_ID = "key-val";
	private static final String JIRA_ISSUE_PAGE_BASE_URL = "https://jira.atlassian.com/browse/";
	private static final String XPATH_EXPR_DESCRIPTION = ".//*[@id='description-val']/div/p";
	private static final String SUMMARY_VAL_ID = "summary-val";
	private static final String SUMMARY_EDIT_ID = "summary";
	private static final String PRIORITY_ID = "priority-field";
	private static final String ISSUE_TYPE_ID = "issuetype-field";
	
	private WebDriver webDriver;

	public JiraIssuePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public void open(String issueKey) {
		webDriver.navigate().to(JIRA_ISSUE_PAGE_BASE_URL.concat(issueKey));
	}
	
	public String getIssueKey() {
		return webDriver.findElement(By.id(KEY_VAL_ID)).getText();
	}
	
	public String getSummary() {
		return webDriver.findElement(By.id(SUMMARY_VAL_ID)).getText();
	}
	
	public String getDescription() {
		return webDriver.findElement(By.xpath(XPATH_EXPR_DESCRIPTION)).getText();
	}
	
	public String getPriority() {
		return webDriver.findElement(By.xpath(ISSUE_PRIORITY_VAL_ID)).getText();
	}
	
	public String getType() {
		return webDriver.findElement(By.xpath(ISSUE_TYPE_VAL_ID)).getText();
	}
	
	public void edit(String editedSummary, String issueType, String priority) {
		webDriver.findElement(By.id(EDIT_ISSUE_LINK)).click();
		WebDriverWait webDriverWait = new WebDriverWait(webDriver, 15);
		
		webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(SUMMARY_EDIT_ID)));
		
		webDriver.findElement(By.id(SUMMARY_EDIT_ID)).sendKeys(editedSummary);
		
		String del = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;
		webDriver.findElement(By.id(ISSUE_TYPE_ID)).sendKeys(del + issueType);
		webDriver.findElement(By.id(PRIORITY_ID)).sendKeys(del + priority);
		
		webDriver.findElement(By.id(EDIT_ISSUE_SUBMIT_LINK)).click();
	}
	
}
