package com.sriraj.selenium.exercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class ExistingIssueSearchTest extends TestBase  {

	@Test
	public void testExistingIssueSearch() {
		ProjectPage projectPage = loginToJiraProject();
		String issueKey = projectPage.createNewIssue(ISSUE_SUMMARY, ISSUE_DESCRIPTION);
		assertNotNull(issueKey);
		
		JiraIssuePage jiraIssuePage = projectPage.search(issueKey);
		assertEquals(issueKey, jiraIssuePage.getIssueKey());
	}
}
