package com.sriraj.selenium.exercise;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CreateNewIssueTest.class, EditExistingIssueTest.class,
		ExistingIssueSearchTest.class })
public class JiraTests {

}
