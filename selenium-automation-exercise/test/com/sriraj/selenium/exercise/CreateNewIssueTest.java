package com.sriraj.selenium.exercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class CreateNewIssueTest extends TestBase {

	@Test
	public void testWithValidSummary() {
		String newIssueKey = createNewJiraIssue();
		assertNotNull(newIssueKey);
		
		JiraIssuePage jiraIssuePage = openJiraIssuePage(newIssueKey);
		String actualSummaryValue = jiraIssuePage.getSummary();
		String actualDescriptionValue = jiraIssuePage.getDescription();
		
		assertEquals(ISSUE_SUMMARY, actualSummaryValue);
		assertEquals(ISSUE_DESCRIPTION, actualDescriptionValue);
	}

	@Test
	public void testWithBlankSummary() {
		ProjectPage projectPage = loginToJiraProject();
		projectPage.createNewIssue("", ISSUE_DESCRIPTION);
		assertEquals(BLANK_SUMMARY_ERROR, projectPage.getErrorMessage());
	}
}
