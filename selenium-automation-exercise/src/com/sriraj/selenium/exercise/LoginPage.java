package com.sriraj.selenium.exercise;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	private static final String USERNAME_F = "username";
	private static final String PASSWORD_F = "password";
	private static final String LOGIN_PAGE_URL = "https://id.atlassian.com/login/login?application=jac&continue=https://jira.atlassian.com/browse/TST";
	private WebDriver webDriver;
	
	public LoginPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public ProjectPage login(String username, String password) {
		webDriver.navigate().to(LOGIN_PAGE_URL);
		webDriver.findElement(By.id(USERNAME_F)).sendKeys(username);
		webDriver.findElement(By.id(PASSWORD_F)).sendKeys(password);
		webDriver.findElement(By.id("login-submit")).click();
		
		return new ProjectPage(webDriver);
	}

}
