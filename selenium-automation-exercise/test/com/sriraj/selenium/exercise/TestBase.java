package com.sriraj.selenium.exercise;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBase {

	protected static final String JIRA_PASSWORD = "P1ece1@#4";
	protected static final String JIRA_USERNAME = "sreeraj.a@gmail.com";
	protected static final String BLANK_SUMMARY_ERROR = "You must specify a summary of the issue.";
	protected static final String ISSUE_DESCRIPTION = "SA Automation Exercise Description";
	protected static final String ISSUE_SUMMARY = "SA Automation Exercise";
	protected static final String EDITED_ISSUE_SUMMARY = "Edited SA Automation Exercise";
	protected WebDriver webDriver;

	public TestBase() {
		super();
	}

	@Before
	public void setUp() throws Exception {
		webDriver = new FirefoxDriver();
	}

	@After
	public void tearDown() throws Exception {
		webDriver.close();
	}

	protected String createNewJiraIssue() {
		ProjectPage projectPage = loginToJiraProject();
		String newIssueKey = projectPage.createNewIssue(ISSUE_SUMMARY, ISSUE_DESCRIPTION);
		return newIssueKey;
	}

	protected JiraIssuePage openJiraIssuePage(String newIssueKey) {
		JiraIssuePage jiraIssuePage = new JiraIssuePage(webDriver);
		jiraIssuePage.open(newIssueKey);
		return jiraIssuePage;
	}

	protected ProjectPage loginToJiraProject() {
		LoginPage loginPage = new LoginPage(webDriver);
		ProjectPage projectPage = loginPage.login(JIRA_USERNAME, JIRA_PASSWORD);
		return projectPage;
	}

}