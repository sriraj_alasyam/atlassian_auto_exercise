Test Environment:
OS: Windows 8.1 64-bit
Java: JDK SE 8
Browser: Firefox ver 32.0.2
Eclipse: Eclipse IDE for Java Developers (Version: Luna Release (4.4.0))
Other third-party libraries: junit-4.11.jar; hamcrest-core-1.3.jar; selenium-server-standalone-2.43.1.jar;

Test cases to be covered:
- New issue creation
- Updating an existing issue
- Searching for an existing issue

Assumptions:
- Using the Atlassian's public test project https://jira.atlassian.com/browse/tst
- Negative test scenarios are not covered.
- Login validations are not performed.
- Focused on task achievement for the above test cases only.


Steps to execute:
1. Import project 'selenium-automation-exercise' into an Eclipse workspace
2. Run JiraTests test suite (Run As > JUnit Test)


Test case 1: Create a new issue in jira (CreateNewIssueTest.java)
- Logs into jira, and goes to https://jira.atlassian.com/browse/tst project page.
- Creates a new issue in jira with predetermined values for summary and description
- It would return a jira issue page when successful or displays an error message while creating when the mandatory 'summary' is empty.

Test case 2: Updating an existing jira (EditExistingIssueTest.java)
- Logs into jira, and goes to https://jira.atlassian.com/browse/tst project page.
- Creates a new issue in jira with predetermined values for summary and description.
- Opens the created issue and edits its Summary, Priority and Issue type to predetermined values.

Test case 3: Search for an existing issue via jira search (ExistingIssueSearchTest.java)
- Logs into jira, and goes to https://jira.atlassian.com/browse/tst project page.
- Creates a new issue in jira with predetermined values for summary and description, and gets the issue id.
- Pass on the issue id to jira's search and open the issue page.
 


